const Course = require("../models/Course");
const User = require("../models/User");

// Create a new course
/*
  Steps:
  1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
  2. Save the new Course to the database
*/
module.exports.addCourse = (reqBody, userData) => {

  return User.findById(userData.userId)
    .then(result => {
      if(result.isAdmin === true){

        // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
        // Uses the information from the request body to provide all the necessary information
        let newCourse = new Course({
          name : reqBody.name,
          description : reqBody.description,
          price : reqBody.price
        });

        newCourse.save()
        
        return true;
      } else {
        return false;
      }
    }).catch(err => err); 
};

// Retrieve all courses
/*
  Steps:
  1. Retrieve all the courses from the database
*/
module.exports.getAllCourses = (userData) => {
  return User.findById(userData.userId)
    .then(result => {
      if(result.isAdmin === true){

        return Course.find({});
      } else {
        return false;
      }
    }).catch(err => err); 
};

// Retrieve all active courses
module.exports.getAllActive = () => {

  return Course.find({ isActive : true })
    .then(result => result)
    .catch(err => err);
};

// Retrieving a specific course
/*
  Steps:
  1. Retrieve the course that matches the course ID provided from the URL
*/
module.exports.getCourse = (reqParams) => {

  return Course.findById(reqParams.courseId)
    .then(result => result)
    .catch(err => err);
};

// Update a course
/*
  Steps:
  1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
  2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {

  // Specify the fields/properties of the document to be updated
  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  }
  /*
    Syntax:
      ModelName.findByIdAndUpdate(documentId, updatesToBeApplied).then(statement)
  */
  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
    .then(course => true).catch(err => err); 
};

// ========= Activity =========
// Update a specific property in a course
module.exports.updatePropertyValueCourse = (reqParams, reqBody) => {

  // Specify the fields/properties of the document to be updated
  let updatedSpecificPropertyCourse = {
    isActive: reqBody.isActive
  }

  return Course.findByIdAndUpdate(reqParams.courseId, updatedSpecificPropertyCourse)
    .then(course => true).catch(err => err); 
};

