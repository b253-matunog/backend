// console.log("hello");

const url = "https://jsonplaceholder.typicode.com/todos";

fetch(url)
  .then(response => response.json())
  .then(json => {
    const titleProperty = json.map(arrayElement => arrayElement.title)
    const array = [titleProperty];
    console.log(array)
  })

// Getting a specific item
fetch("https://jsonplaceholder.typicode.com/todos/1")
  .then(response => response.json())
  .then(json => {
    console.log(json);
    let message = `The item "${json.title}" on the list has a status of ${json.completed}`;
    console.log(message)
  });

// Posting an item
fetch(url, {
  method: "POST",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "Created To Do List Item",
    completed: false,
    userId: 1
  })
})
  .then(response => response.json())
  .then(json => console.log(json));

// Updating an item
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "Updated To Do List Item",
    description: "To update the my to do list with a different data structure",
    status: "Pending",
    dateCompleted: "07/09/21",
    userId: 1
  })
})
  .then(response => response.json())
  .then(json => console.log(json));

// Updating an item using PATCH method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    status: "Complete",
    dateCompleted: "07/09/21"
  })
})
  .then(response => response.json())
  .then(json => console.log(json));

// Deleting an item
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: "DELETE"
});