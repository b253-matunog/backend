// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// Insert Documents (Create)
/*
  Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

  Insert Many Documents
    db.collectionName.insertMany([
      {
        "fieldA": "valueA",
        "fieldB": "valueB"
      },
      {
        "fieldA": "valueA",
        "fieldB": "valueB"
      }
    ]);
*/
db.users.insertOne({
  "firstName": "Arren Jade",
  "lastName": "Matunog",
  "mobileNumber": "+639123456789",
  "email": "edajnerra328@gmail.com",
  "company": "Zuitt"
});

db.users.insertMany([
  {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
      phone: "87654321",
      email: "stephenhawking@mail.com",
    },
    courses: ["Python", "React", "PHP", "CSS"],
    department: "none"
  },
  {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
      phone: "98765432",
      email: "neilarmstrong@mail.com"
    },
    courses: ["React", "Laravel", "Sass"],
    department: "none"
  }
]);

// Finding documents (Read/Retrieve)
/*
  Syntax:
    - db.collectionName.find(); - find all
    - db.collectionName.find({field: value});
    - db.collectionName.findOne({field: value}) - first document that will match the criteria
    - db.collectionName.findOne({}) - find first document
*/
db.users.find();
db.users.find({"firstName": "Arren Jade"});
db.users.findOne({});

//Mini Activity
/*
  1. Make a new collection with the name "courses"
  2. Insert the following fields and values

  name: Javascript 101
  price: 5000
  description: Introduction to Javascript
  isActive: true

  name: HTML 101
  price: 2000
  description: Introduction to HTML
  isActive: true
*/

db.courses.insertMany([
  {
    name: "JavaScript 101",
    price: "5000",
    description: "Introduction to JavaScript",
    isActive: true
  },
  {
    name: "HTML 101",
    price: 2000,
    description: "Introduction to HTML",
    isActive: true
  }
]);

// Updating/Replacing/Modifying documents (update)
/*
  Syntax:
  Updating One Document
    db.collectionName.updateOne(
      {
        field: value
      },
      {
        $set: {
          fieldToBeUpdated: value
        }
      }
    );
      - update the first matching document in our collection

    Multiple/Many Documents
    db.collectionName.updateMany(
      {
        field: value
      },
      {
        $set: {
          fieldToBeUpdated: value
        }
      }
    );
      update multiple document that match the criteria
*/

db.users.insertOne({
  "firstName": "Test",
  "lastName": "Test",
  "mobileNumber": "+639123456789",
  "email": "test@gmail.com",
  "company": "none"
});

db.users.updateOne(
  {
    "firstName": "Test",
  },
  {
    $set: {
      "firstName": "Bill",
      "lastName": "Gates",
      "mobileNumber": "123456789",
      "email": "billgates@mail.com",
      "company": "Microsoft",
      "status": "active"
    }
  }
);

/*
	Mini Activity:
		- Use updateOne() to change the isActive status of a course into false.
		-Use the updateMany() to set and add “enrollees” with a value of 10.
*/

db.courses.updateOne(
  {
    "firstName": "HTML 101",
  },
  {
    $set: {
      "isActive": false
    }
  }
);

db.courses.updateMany(
  {},
  {
    $set: {
      "enrollees": 10
    }
  }
)

// Remove Field
db.users.updateOne(
  {
    "firstName": "Bill"
  },
  {
    $unset: {
      "status": "active"
    }
  }
);

/*
	Syntax:
		Deleting One Document:
			db.collectionName.deleteOne({"critera": "value"})

		Deleting Multiple Documents:
			db.collectionName.deleteMany({"criteria": "value"})
*/

db.users.deleteOne(
  {
    "company": "Microsoft"
  }
);

db.users.deleteMany({
  "department": "none"
});

db.users.deleteMany({});