const bcrypt = require("bcrypt");
// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");

// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

  // The result is sent back to the frontend via the "then" method found in the route file
  return User.find({ email : reqBody.email }).then(result => {

    // The "find" method returns a record if a match is found
    if(result.length > 0){

      return true;
    
    // No duplicate email found
    // The user is not yet registered in the database
    } else {
      
      return false;
    }
  }).catch(err => err);
};

//============= User registration ===============
module.exports.registerUser = (reqBody) => {

  // Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
  // Uses the information from the request body to provide all the necessary information
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    // 10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
    password: bcrypt.hashSync(reqBody.password, 10)
  });

  return newUser.save().then(user => user ? true : false).catch(err => err)
};

//============== User authentication =================
module.exports.loginUser = (reqBody) => {

  // The "findOne" method returns the first record in the collection that matches the search criteria
  // We use the "findOne" method instead of the "find" method which returns all records that match the search criteria
  return User.findOne({ email : reqBody.email })
    .then(result => {
      // user does not exist
      if(result == null){
        return false;
      // user exist
      } else {
        // Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
        // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

        if(isPasswordCorrect){

          return { access : auth.createAccessToken(result) };

        // Passwords do not match
        } else {

          return false;
        }
      }
    }).catch(err => err);
};

//============== User details ================
module.exports.getProfile = (data) => {
  return User.findById(data.userId)
    .then(result => {
      if(result){
        result.password = "";
        return result;
      } else {
        return false;
      }
    }).catch(err => err); 
};

// Order product/s
module.exports.createOrder = async (req, res) => {
  
  const userData = auth.decode(req.headers.authorization);
  const { productId, productName, quantity } = req.body;

  try {
    const user = await User.findById(userData.id);
    if (!user) {
      return res.status(404).json({ error: 'User not found' }); // 
    }

    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ error: 'Product not found' });
    }

    // Add order to user document
    user.orderedProduct.push({
      products: [
        {
          productId: productId,
          productName: productName,
          quantity: quantity,
        },
      ],
      totalAmount: quantity * product.price,
    });

    await user.save();

    // Add user to product document
    product.userOrders.push({ userId: userData.id });
    await product.save();

    return res.status(201).json({ message: 'Order created successfully' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: 'Server error' });
  }
};

// Retrieve all orders
// module.exports.getAllOrders = async (req, res) => {

//   const userData = auth.decode(req.headers.authorization);

//   try {
//     const user = await User.findById(userData.id);
//     if (!user) {
//       return res.status(404).json({ error: 'User not found' }); // 
//     }

//     if(userData.isAdmin){
//       const orders = await Product.find({}).populate('userOrders');
//       return res.json(orders);
//     } else {
//       return false;
//     }
//   } catch (err) {
//     console.error(err);
//     res.status(500).send('Server Error');
//   }
// };