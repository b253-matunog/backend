const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res) => {

  // The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
  // The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
  userController.checkEmailExists(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err));
});

// Route for user registration
router.post("/register", (req, res) => {
  userController.registerUser(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err));
});

// Route for user login
router.post("/login", (req, res) => {
  userController.loginUser(req.body)
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err));
})

// Route for retrieving user details
router.get("/userDetails", (req, res) => {

  // Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
  const userData = auth.decode(req.headers.authorization)

  userController.getProfile({ userId : userData.id })
    .then(resultFromController => res.send(resultFromController))
    .catch(err => res.send(err));
});

// Route to create order (Non-admin)
router.post("/checkout", auth.verify, userController.createOrder);

// Route to get All orders
// router.get("/orders", auth.verify, userController.getAllOrders);


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;