const express = require("express");
const mongoose = require("mongoose");

// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");
// Allows access to routes defined within our application
const userRoute = require("./routes/userRoute")
const productRoute = require("./routes/productRoute");

// Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();
const port = process.env.PORT || 4000;
const db = mongoose.connection;

// Connect to MongoDB Database
mongoose.connect("mongodb+srv://admin:admin123@batch253-matunog.lfjfa0e.mongodb.net/capstone2-ecommerceAPI?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

db.once("open", () => console.log("Now connected to MongoDB Atlas"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/api/users", userRoute);
// Defines the "/products" string to be included for all course routes defined in the "product" route file
app.use("/api/products", productRoute);

if(require.main === module){
	app.listen(port, () => {
		console.log(`API is now online on http://localhost:${ port }`)
	})
}