db.users.insertMany([
  {
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
      phone: "098765432",
      email: "janedoe@mail.com",
    },
    courses: ["HTML", "Bootstrap", "CSS"],
    department: "HR"
  },
  {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
      phone: "87654321",
      email: "stephenhawking@mail.com",
    },
    courses: ["Python", "React", "PHP", "CSS"],
    department: "HR"
  },
  {
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    contact: {
      phone: "98765432",
      email: "neilarmstrong@mail.com"
    },
    courses: ["React", "Laravel", "Sass"],
    department: "HR"
  }
]);

// finding the letter s in their first name or d in their last name
db.users.find(
  {
    $or: [
      {
        "firstName": { 
          $regex: 's',
          $options: '$i'
        }
      },
      {
        "lastName": {
          $regex: 'd',
          $options: '$i'
        }
      }
    ]
  },
  {
    "_id": 0,
    "firstName": 1,
    "lastName": 1
  }
);

// finding the users who are from HR department and their age is greater than or equal to 70.
db.users.find({
  $and: [
    { "department": "HR" },
    { "age": { $gte: 70 } }
  ]
});

// finding users with the letter e in their first name and has an age of less than or equal to 30
db.users.find({
  $and: [
    {
      "firstName": { 
        $regex: 'e',
        $options: '$i'
       } 
    },
    {
      "age": { $lte: 30 }
    }
  ]
});