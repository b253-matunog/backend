// console.log("Hello World!");

function addNum(numA, numB){
  let sum = numA + numB;
  console.log(sum);
}
console.log("Displayed sum of 5 and 15");
addNum(5, 15);

function subNum(numA, numB){  
  let difference = numA - numB;
  console.log(difference);
};
console.log("Displayed difference of 20 and 5")
subNum(20, 5);

function multiplyNum(numA, numB){
  let product = numA * numB;
  console.log("The product of " + numA + " and " + numB + ":");
  return product;
};
let product = multiplyNum(50, 10);
console.log(product);

function divideNum(numA, numB){
  let quotient = numA / numB;
  console.log("The quotient of " + numA + " and " + numB + ":");
  return quotient;
};
let quotient = divideNum(50, 10);
console.log(quotient);

function getCircleArea(radius){
  let area = 3.1416 * radius ** 2;
  console.log("The result of getting the area of a circle with " + radius + " radius:");
  return area;
};
let circleArea = getCircleArea(15);
console.log(circleArea);

function getAverage(numA, numB, numC, numD){
  let sum = numA + numB + numC +numD;
  let average = sum / 4;
  console.log("The average of " + numA + ", " + numB + ", " + numC + " and " + numD + ":");
  return average;
};

let averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar);

function checkIfPassed(score, totalScore){
  let percentage = score / totalScore;
  let checkIfPass = percentage >= .75;
  console.log("Is " + score + "/" + totalScore + " a passing score?")
  return checkIfPass;
};

let isPassingScore = checkIfPassed(38, 50);
console.log(isPassingScore);




//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}