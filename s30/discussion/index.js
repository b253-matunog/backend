db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

// MongoDB Aggregation Method
// Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data

// Single-Purpose Aggregation Operations
// If we only need simple aggregations with only 1 stage
db.fruits.count();

/*
  - The "$match" is used to pass the documents that meet the specified conditionns
    Syntax:
      {
        $match: { "field": "value" } 
      }
      
  - The "$group" is used to group the element together and field-value pairs using the data from the grouped elements
    Syntax:
      { 
        $group: {
          "_id": "value",
          "fieldResult": "valueResult"
        } 
      }
*/

// Pipelines with multiple stages
db.fruits.aggregate([
  {
    $match: {
      "onSale": true
    }
  },
  {
    $group: {
      "_id": "$supplier_id", // group by "supplier_id"
      "total": { $sum: "$stock" } // total values of all "stocks"
    }
  }
]);

// Field Projection with Aggregation
// $project can be used aggregating data to include/exclude field from the returned results
/*
  Syntax:
    { $project: { "field": 1/0 } }
*/

db.fruits.aggregate([
  { $match: { "onSale": true } },
  { 
      $group: { 
      "_id": "$supplier_id",
      "total": { 
        $sum: "$stock"
      }
    } 
  },
  {
    $project: { "_id": 0 }
  }
]);

// MINI-ACTIVITY
/*
	aggeregate all fruits that has a stock greater than or equal 20
	group by supplier id 
	show the sum of all stocks
*/

db.fruits.aggregate([
  {
    $match: { "stock": { $gte: 20 } } 
  },
  { 
    $group: { 
      "_id": "$supplier_id",
      "total": { 
        $sum: "$stock"
      }
    } 
  },
]);


// Sorting Aggregated Results
/*
	- The "$sort" can be used to change the order of aggregated results
	- Providing a value of -1 will sort the aggregated results in a reverse order
    Syntax:
      { $sort: { "field": 1/-1 } }
*/

db.fruits.aggregate([
  {
    $match: { "onSale": true }
  },
  {
    $group: {
      "_id": "$supplier_id",
      "total": {
        $sum: "$stock"
      }
    }
  },
  {
    $sort: {
      "total": -1
    }
  }
]);

// Computation for aggregated results
// used in $group staging
/*
  $sum - to total the values
  $avg - to total the average of values
  $min - minimum value of the field
  $max - maximum value of the field

  Syntax:
    { 
      $group: { 
        "_id": "fieldToBeGrouped",
        "fieldForResults": { $sum/max/min/avg: "$fieldToGetResults"} 
      }
    }
*/

// Aggregating results based on array fields
/*
  - The "$unwind" deconstructs an array field from a collection/field with an array value to output a result for each element.

  Syntax:
    {
      $unwind: "Sfield"
    }
*/

db.fruits.aggregate([
  {
    $unwind: "$origin"
  }
]);

db.fruits.aggregate([
  {
    $unwind: "$origin"
  },
  {
    $group: {
      "_id": "$origin",
      "kinds": {
        $sum: 1
      }
    }
  }
]);

// ======== Schema Design ========
let owner = ObjectID();

db.owners.insertOne({
  "_id": owner,
  "name": "John Smith",
  "Contact": "09123456789"
});

// Referenced Data Model
db.suppliers.insertOne({
  "name": "ABC fruits",
  "contact": "09123456789"
  // "owner_id": <owner_id>
});

// Embedded Data Model
db.suppliers.insertOne({
  "name": "DEF Fruits",
  "contact": "09123456789",
  "address": [
    {
      "street": "360 San Jose",
      "city": "Manila"
    },
    {
      "street": "367 Gil Puyat",
      "city": "Makati"
    }
  ] 
});

// Reference Links
/*
  MongoDB $match
		https://docs.mongodb.com/manual/reference/operator/aggregation/match/
	MongoDB $group
		https://docs.mongodb.com/manual/reference/operator/aggregation/group/
	MongoDB $sum
		https://docs.mongodb.com/manual/reference/operator/aggregation/sum/
  MongoDB $project
		https://docs.mongodb.com/manual/reference/operator/aggregation/project/
	MongoDB $sort
		https://docs.mongodb.com/manual/reference/operator/aggregation/sort/
	MongoDB $unwind
		https://docs.mongodb.com/manual/reference/operator/aggregation/unwind/
	MongoDB $count
		https://docs.mongodb.com/manual/reference/operator/aggregation/count/
	MongoDB $avg
		https://docs.mongodb.com/manual/reference/operator/aggregation/avg/
	MongoDB $min
		https://docs.mongodb.com/manual/reference/op
*/