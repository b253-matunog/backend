// Exponent Operator
let getCube = 2 ** 3;

// Template Literals
console.log(`The cube of 2 is ${getCube}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

let [houseNumber, street, state, zipCode] = address;

console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => console.log(number));

// reduce()
let reduceNumber = numbers.reduce((acc, curr) => {
  // operation to return/reduce the array into a single value
  return acc + curr;
})

console.log(reduceNumber);

// Javascript Classes
class Dog{
  constructor(name, age, breed){
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);


//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}