// console.log("Hello World!");

/* ES6 UPDATES */
// Exponent Operator

// ES6 update
const firstNum = 8**2;
console.log(firstNum);
// old javascript
const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template literals
/*
  - allows us to write strings without using the concatication operator (+)
  - Greatly helps with code readability
*/

let name = "John";

// Pre-template Literal
let message = "Hello " + name + "! Welcome to programming!";
console.log(message);

// String using Template Literals
// uses backticks (``);
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with Template Literals: ${message}`);

// Multi-line using Template Literals
let anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`;
console.log(anotherMessage);

/*
  - Template literals allow us to write strings with embedded JavaScript expressions
  - expressions are any valid unit of code that resolves to a value
  - "${}" are used to include JavaScript expressions in strings using template literals
*/

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is : ${principal * interestRate}`);

// Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability

  Syntax:
    let/const [variableName1, variableName2, variableName3] * arrayName;
*/

const fullName = ["Juan", "Dela", "Cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`)

// Array Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

console.log(`Hello ${fullName}! It's nice to meet you!`);

// Object Destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects

  Syntax:
    let/const {propertyNameA, propertyNameB, properNameC} = objectName;
*/

const person = {
  givenName: "Juana",
  maidenName: "Dela",
  familyName: "Cruz"
}

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`);

// Object Destructuring
const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

// to rename our property names
// const {givenName:fName, maidenName:mName, familyName:lName} = person; 

// console.log(fName);
// console.log(mName);
// console.log(lName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`);

/*
  Passing the values using (function) destructured properties of the object
  Syntax:
    function funcName({propertyName, propertyName, propertyName}){
      return statement (parameters)
    }
    funcName(object);
*/
// Function Destructuring
function getFullName({givenName, maidenName, familyName}){
  console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// Arrow Function
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
  - Arrow functions also have implicit return which means we don't need to use the "return" keyword
*/

// function printFullName(firstName, lastName){
//   console.log(`${firstName} ${lastName}`);
// }
// printFullName("John", "Smith");

// Arrow Function (=>)
/*
  Syntax:
    let/const variableName = (parameterA, parameterB) =>{
      statement;
    }

    or (if single-line)

    let/const variableName = () => statement;
*/
const printFullName = (firstName, lastName) => {
  console.log(`${firstName} ${lastName}`);
}
printFullName("Jane", "Smith");

const hello = () => "Hello";
console.log(hello());

const sum = (x, y) => x + y;
let total = sum(1,1);
console.log(total);

person.talk = () => "Helloooooo!";
console.log(person.talk()); 

// Function with default Argument Value
const greet = (name = "User") => {
  return `Good morning, ${name}`
}

console.log(greet());
console.log(greet("Ariana"));

// Class-Based OBject Blueprints
/*
  Allows the creation/instantation of the objects using classes as blueprints

  Syntax:
    class className {
      constructor (objectPropertyA, objectPropertyB){
        this.objectPropertyA = objectPropertyA;
        this.objectPropertyB = objectPropertyB;
      }
    };
*/

class Car {
  constructor(brand, model, year){
    this.brand = brand;
    this.model = model;
    this.year = year;
  }
}

const myCar = new Car();
console.log(myCar);

// Values of properties may be assigned after creation/instantiation of object
myCar.brand = "Ford";
myCar.model = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);